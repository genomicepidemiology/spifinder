SPIFinder
===================

This project documents the SPIFinder service


Documentation
=============

The SPIFinder service contains one python script SPIFinder.py which is the script of the latest
version of the SPIFinder service. SPIFinder identifies Salmonella Pathogenetic Islands in total or partial sequenced isolates of bacteria.


## Content of the repository
1. SPIFinder.py      - the program
2. README.md


## Installation

Setting up SPIFinder program
```bash
# Go to wanted location for spifinder
cd /path/to/some/dir
# Clone and enter the spifinder directory
git clone https://bitbucket.org/genomicepidemiology/spifinder.git
cd spifinder
```

#Download and install SPIFinder database

```bash
# Go to the directory where you want to store the SPIfinder database
cd /path/to/some/dir
# Clone database from git repository (develop branch)
git clone https://bitbucket.org/genomicepidemiology/spifinder_db.git
cd spifinder_db
SPIFINDER_DB=$(pwd)
# Install SPIFinder database with executable kma_index program
python3 INSTALL.py kma_index
```

If kma_index has no bin install please install kma_index from the kma repository:
https://bitbucket.org/genomicepidemiology/kma

## Usage

The program can be invoked with the -h option to get help and more information of the service. The following options are available when running SPIFinder:

`-i INPUTFILE	input file (fasta or fastq) relative to pwd, up to 2 files`

`-o OUTDIR	output directory relative to pwd`

`-d DATABASE    set a specific database`

`-p DATABASE_PATH    set path to database, default is spifinder_db`

`-mp METHOD_PATH    set path to method (blast or kma)`

`-l MIN_COV    set threshold for minimum coverage`

`-t THRESHOLD set threshold for mininum blast identity`

`-tmp    temporary directory for storage of the results from the external software`

`-x    extended output: Give extented output with allignment files, template and query hits in fasta and a tab seperated file with gene profile results`

`-q    don't show results `

## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/SPIFinder/

Citation
=======
For publication of results, please cite:

Is the Evolution of Salmonella enterica subsp. enterica Linked to Restriction-Modification Systems?
Roer, L., Hendriksen, R. S., Leekitcharoenphon, P., Lukjancenko, O., Kaas, R. S., Hasman, H., and Aarestrup, F. M.
mSystems 1(3):e00009-16.
https://doi.org/10.1128/mSystems.00009-16

References
=======

1. Camacho C, Coulouris G, Avagyan V, Ma N, Papadopoulos J, Bealer K, Madden TL. BLAST+: architecture and applications. BMC Bioinformatics 2009; 10:421.
2. Clausen PTLC, Aarestrup FM, Lund O. Rapid and precise alignment of raw reads against redundant databases with KMA. BMC Bioinformatics 2018; 19:307.

License
=======
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.